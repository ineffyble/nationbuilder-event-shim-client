var set_calendar;

update_set_calendar = function(e) {
	if (new_calendar = document.getElementById('rsvp_calendar').value) {
		if (new_calendar != set_calendar) {
			chrome.storage.local.set({
				'set_calendar': new_calendar
			});
			set_calendar = new_calendar;
			shim_the_event_list();
		}
	}
};

shim_the_event_list = function() {
    eventsrequest = new XMLHttpRequest();
    eventsrequest.onreadystatechange = function() {
        if (eventsrequest.readyState == 4 && eventsrequest.status == 200) {
            events = JSON.parse(eventsrequest.responseText);

            if (!document.querySelector('#rsvp_event')) {
	            event_select = document.createElement('select');
                event_select.setAttribute("id", "rsvp_event");
                rsvp_form.appendChild(event_select);
            } else {
            	event_select = document.querySelector("#rsvp_event");
            }


            html = '';
            first = true;
            for (var event in events) {
                id = events[event];
                html += '<option value="' + id;
                if (first) {
                	html += '" selected="selected';
                	first = false;
                }
                html += '">' + event + '</option>';
            }
            event_select.value = '';
            event_select.innerHTML = html;
            if (chosen = document.querySelector("#s2id_rsvp_event .select2-chosen")) {
            	chosen.innerHTML = '-Choose Event-';
            }
        }
    };
    if (set_calendar) {
	    eventsrequest.open('GET', server + '/events?calendar_id=' + set_calendar);
    } else {
	    eventsrequest.open('GET', server + '/events');
    }
    eventsrequest.send(null);	
};

add_the_calendar_select = function() {
   calendarsrequest = new XMLHttpRequest();
    calendarsrequest.onreadystatechange = function() {
        if (calendarsrequest.readyState == 4 && calendarsrequest.status == 200) {
            calendars = JSON.parse(calendarsrequest.responseText);
            calendar_select = document.createElement('select');
            calendar_select.setAttribute("id", "rsvp_calendar");
            html = '';
            for (var calendar in calendars) {
                id = calendars[calendar];
                html += '<option value="' + id;
                if (set_calendar == id) {
                	html += '" selected="selected';
                }
                html += '">' + calendar + '</option>';
            }
            html += '';
            calendar_select.innerHTML = html;
            rsvp_form.appendChild(calendar_select);
			rsvp_form.addEventListener('DOMSubtreeModified', update_set_calendar, false);
        }
    };
    calendarsrequest.open('GET', server + '/calendars');
    calendarsrequest.send(null);
}

make_the_button = function() {
	button = document.createElement('button');
	button.setAttribute("id", "rsvp_button");
	button.innerHTML = "RSVP";
	rsvp_form.appendChild(button);
	button.addEventListener('click', send_rsvp, false);
}

send_rsvp = function(e) {
	event = document.querySelector('#rsvp_event').value;
	person = window.location.pathname.split("/")[3];
	rsvprequest = new XMLHttpRequest();
	rsvprequest.open('GET', server + '/rsvp?person_id=' + person + '&event_id=' + event);
	rsvprequest.send(null);
	button.setAttribute("style", "background-color: #39b54a;");
	button.innerHTML = "RSVPed!";
	e.preventDefault();
}

if (document.querySelector("[name='event_rsvp[page_id]']")) {
    if (!server) {
        required_settings = ['server', 'set_calendar'];
    } else {
        required_settings = ['set_calendar'];
    }

    chrome.storage.local.get(required_settings, function(result) {
        if (result.server) {
            server = result.server;
        }
        if (result.set_calendar) {
            set_calendar = result.set_calendar;
        }
        rsvp_select = document.querySelector("[name='event_rsvp[page_id]']");
        rsvp_form = document.querySelector("#new_event_rsvp");
        rsvp_form.innerHTML = '';

        shim_the_event_list();
        add_the_calendar_select();
        make_the_button();
    });
}