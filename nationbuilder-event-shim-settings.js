var server_is_configurable;
var server;
var set_calendar;

init_form = function() {
	server_input = document.getElementById('server');

	if (server_is_configurable) {
		server_input.value = server;
	} else {
		server_input.remove();
	}

	if (server) {
		get_calendars();
	}

	form = document.getElementById('settings');
	form.addEventListener("submit", save_settings, false);
};

save_settings = function(e) {
	if (calendar_select.value) {
		chrome.storage.local.set({
			'set_calendar': calendar_select.value
		});
	}
	if (server_input.value) {
		chrome.storage.local.set({
			'server': server_input.value
		});		
	}
	e.preventDefault();
};

get_calendars = function() {
	request = new XMLHttpRequest();
	request.onreadystatechange = function() {
		if (request.readyState == 4 && request.status == 200) {
			calendars = JSON.parse(request.responseText);
			make_calendar_select(calendars);
		}
	};
	request.open('GET', server + '/calendars');
	request.send(null);

};

make_calendar_select = function(calendars) {
	html = '';
	for (var calendar in calendars) {
		id = calendars[calendar];
		html += '<option value="' + id + '"'; 
		if (id == set_calendar) {
			html += 'selected="selected"';
		}
		html += ' >' + calendar + '</option>';
	}
	calendar_select = document.getElementById('calendar');
	calendar_select.innerHTML = html;
};

if (!server) {
	required_settings = ['server', 'calendar'];
  	server_is_configurable = true;
} else {
	required_settings = ['calendar'];
}

chrome.storage.local.get(required_settings, function(result) {
if (result.server) {
	server = result.server;
}
if (result.set_calendar) {
	set_calendar = result.set_calendar;
}

init_form();
});